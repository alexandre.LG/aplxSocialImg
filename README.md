# aplxSocialImg
aplxSocialImg (fork de SocialButtons) : Images de partage sur quelques sociaux

## Ce que ça fait
- une seule et unique image pour les RS
- effet de survol sur les images
- personnalisations de l'affichage en CSS
- image pour : commentaires, google+, facebook, twitter et linkedin
- style défini dans un fichier css
- valide html5 par le w3c
- infobulle sur l'image des commentaires (pour le nombre)

## Le rendu
Voilà en gros ce que ça donne (installez-le pour voir réellement la différence avec SocialButtons) :

![Démo](demo.png)

## Remerciements
- websyys pour m'avoir lancer dans la réalisation de ce plugin
- L'équipe de PluXml pour la documentation très bien fournie
- Stéphane pour son plugin [MySocialButtons](http://forum.pluxml.org/viewtopic.php?id=2924) qui m'a servit de base